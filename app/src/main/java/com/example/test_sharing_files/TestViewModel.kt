package com.example.test_sharing_files

import android.content.Intent
import android.net.Uri
import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.test_sharing_files.data.TestDataItem
import com.example.test_sharing_files.data.TestDataList
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import java.io.File
import java.io.FileOutputStream


const val TAG = "MYVIEWMODEL"
class TestViewModel: ViewModel(){
    private val _testList = MutableStateFlow(TestDataList())
    val testList = _testList.asStateFlow()
    val file: MutableStateFlow<File?> = MutableStateFlow(null)

    fun saveDataToJsonFile(filesDir: File) {
        viewModelScope.launch {

            val json = Json.encodeToString(testList.value)
            val path = File("${filesDir}/MINE")
            path.mkdirs()
            file.update {
                File(path,"testfile.json")
            }
            FileOutputStream(file.value).write(json.toByteArray())

            Log.d(TAG, "saveDataToJsonFile: ${Uri.fromFile(file.value)}")
        }
    }

    fun addFakeData() {

        val exampleQuestion = TestDataItem(
            body = "вопрос 1",
            answers = listOf("ответ 1", "ответ 2"),
            score = 2
        )

        val exampleQuestion2 = TestDataItem(
            body = "вопрос 2",
            answers = listOf("ответ 3", "ответ 4"),
            score = 1
        )

        _testList.update {
            it.copy(
                name = "example name",
                author = "dso",
                questions = it.questions + exampleQuestion + exampleQuestion2
            )
        }
    }

}

