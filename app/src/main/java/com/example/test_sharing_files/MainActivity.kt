package com.example.test_sharing_files

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.size
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.core.content.FileProvider
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.test_sharing_files.data.TestDataList
import com.example.test_sharing_files.ui.theme.Test_sharing_filesTheme
import java.io.File

class MainActivity : ComponentActivity() {



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {

            Test_sharing_filesTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    MainInterface(
                        filesDir = getFilesDir()
                    ) { file: File? ->

                        // TODO: check if file exist

                        if (file == null) {

                            Toast.makeText(
                                applicationContext,
                                "файл не существует",
                                Toast.LENGTH_LONG
                            ).show()

                        } else {
                            val fileUri = FileProvider.getUriForFile(this, "${applicationContext.packageName}.fileprovider", file!!)

                            val intent = Intent(Intent.ACTION_SEND).also {
                                it.setType("application/json")
                                it.putExtra(Intent.EXTRA_STREAM, fileUri)
                            }

                            startActivity(Intent.createChooser(intent, "Отправить через"))
                        }

                    }
                }
            }
        }
    }
}

@Composable
fun MainInterface(filesDir: File, shareChooser: (file: File?) -> Unit) {
    
    val vm = viewModel<TestViewModel>()
    val state by vm.testList.collectAsState()
    val file by vm.file.collectAsState()

    Column(
        modifier = Modifier.fillMaxWidth(),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center
    ) {
        StateText(state)

        Spacer(modifier = Modifier.weight(1f))
        Button(onClick = vm::addFakeData ) {
            Text(text = "Add fake data")
        }
        Spacer(modifier = Modifier.size(32.dp))
        Button(onClick = { vm.saveDataToJsonFile(filesDir) }) {
            Text(text = "Save json file")
        }
        Spacer(modifier = Modifier.size(32.dp))
        Button(onClick = {

            shareChooser(file)

        }) {
            Text(text = "Share json file")
        }
        Spacer(modifier = Modifier.weight(1f))
    }

}

@Composable
fun StateText(
    state: TestDataList
) {

    Row(
        modifier = Modifier.fillMaxWidth(),
        horizontalArrangement = Arrangement.Center,
    ) {
        Text(text = state.toString())
    }
    
}
//
//@Preview(showSystemUi = true)
//@Composable
//fun MainInterfacePreview() {
//    MainInterface(File("")) { intent: Intent ->
//        startActivity(Intent.createChooser(intent, "Отправить через"))
//    }
//}