package com.example.test_sharing_files.data

import kotlinx.serialization.Serializable

@Serializable
data class TestDataList (
    val name: String = "",
    val author: String = "",
    val questions: List<TestDataItem> = emptyList()
)

@Serializable
data class TestDataItem(
    val body: String,
    val score: Int,
    val answers: List<String>,
)



